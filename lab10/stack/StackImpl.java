package stack;
import java.util.ArrayList;
import java.util.List;
public class StackImpl<T> implements Stack<T> {
	StackItem<T> top;
	@Override
	public void push(T item) {
		// TODO Auto-generated method stub	
		StackItem<T> newTop = new StackItem<>(item,top);
		top = newTop;	}
	@Override
	public T pop() {
		// TODO Auto-generated method stub
		T item= top.getItem();
		
		top = top.getPrevious();
		return item;
	}
	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		return top == null;
	}

	@Override
	public List<T> toList() {
		
		ArrayList<T> content = new ArrayList<T>();
		StackItem<T> current = top;
		while (current !=null){
			content.add(0,current.getItem());
			current =  current .getPrevious();	}
		return content;
	}}
