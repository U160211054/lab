package stack;

public class StackItem<T>{
	
	T item;
	 StackItem<T> previous; 
	 
	 public StackItem (T item, StackItem<T> previous ){
		 
		 this.item = item;
		 this.previous = previous;
	 }

	public T getItem() {
		// TODO Auto-generated method stub
		
		return item;
	}

	public StackItem<T> getPrevious() {
		// TODO Auto-generated method stub
		return previous;
	}

}
