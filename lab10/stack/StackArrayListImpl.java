package stack;
import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T> implements Stack<T> {

	private  ArrayList<T> contents = new ArrayList<T>();
	@Override
	public void push(T item) {
		contents.add(item);
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public T pop() {
		
		return contents.remove(contents.size() - 1);
	}

	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		return contents.size() == 0;
	}

	@Override
	public List<T> toList() {
		// TODO Auto-generated method stub
		return contents;
	}
}
