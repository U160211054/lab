 package generics;
import java.util.ArrayList;
import java.util.List;
public class Exercise1 {

	public static void main(String[] args) {
		
		String[] strs = {"a", "b", "c","d"};
		List<String> strList = new ArrayList<>();
		addToCollection(strs,strList);
		
		System.out .println(strList);
		Integer[] ints = {1, 2, 3, 4};
		List<Integer> intList = new ArrayList<>();
		addToCollection(ints,intList);
		
		System.out .println(intList);	
	}

	private static <T>void addToCollection(T[] arr, List<T> List) {
		System.out .println("generics");
		for (T e: arr){
		 List.add(e);
		}	}

	/**private static void addToCollection(String[] strs, List<String> strList) {
		
		System.out .println("string");
		for (String s: strs){
			strList.add(s);
		}  */
	}