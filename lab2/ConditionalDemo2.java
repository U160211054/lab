
public class ConditionalDemo2 {

    public static void main(String[] args){

    	//int value1 = 1;
        //int value2 = 2;
        int result;
	   int value1= Integer.parseInt(args[0]);
	   int value2 = Integer.parseInt(args[1]);

       // boolean someCondition = true;
		boolean someCondition = value1 > value2;

        result = someCondition ? value1 : value2;

        System.out.println(result);	
    }
}
